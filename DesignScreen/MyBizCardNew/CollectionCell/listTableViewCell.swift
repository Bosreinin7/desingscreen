//
//  listTableViewCell.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/28/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class listTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCells(title:String){
        self.titleLbl.text = title
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
