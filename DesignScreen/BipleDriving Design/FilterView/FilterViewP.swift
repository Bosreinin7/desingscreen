//
//  FilterView.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/4/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class FilterViewP: UIView {
    private var superView : UIView!
    //MARK: - life cycle
       override init(frame: CGRect) {
           super.init(frame: frame)
       }
       
       required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
       }
       
    convenience init(xPosition : CGFloat, yPosition : CGFloat, parentView: UIView) {
           initSelf: do {
               self.init(frame: CGRect(x: xPosition, y: yPosition, width: parentView.bounds.width, height: parentView.bounds.height))
               self.superView = parentView
           }
           
           initFilterView: do {
               let filterView = Bundle.main.loadNibNamed("FilterView", owner: self, options: nil)?.first as! UIView
            filterView.frame = self.bounds
               self.addSubview(filterView)
           }
      }
    func show(){
       superView.addSubview(self)
        
    }
    
    @IBAction func closeClicked(_ sender: UIButton) {
        self.removeFromSuperview()
    }
  
    
}
