//
//  UserViewController.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/20/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class UserViewController: UIViewController {
    //MARK:- IBOUTLET------------
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- VARIABLE------------
    let userListVM = UserListVM()
    let activityIdicator = UIActivityIndicatorView(style: .medium)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.fetchUserList()
    }
}
extension UserViewController:UITableViewDataSource,UITableViewDelegate{

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        let data = userListVM.data[indexPath.row]
        cell.configureCell(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userListVM.data.count
    }
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if !userListVM.isLastpage {
            self.activityIdicator.startAnimating()
            self.activityIdicator.color = .blue
            self.tableView.tableFooterView = self.activityIdicator
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let height              = scrollView.frame.height
        let contentYoffset      = scrollView.contentOffset.y
        let distancefrombuttom  = scrollView.contentSize.height - contentYoffset
        if distancefrombuttom < height + 44 {
            if !userListVM.isLastpage {
                      self.fetchUserList()
                      self.tableView.tableFooterView = UIView()
                }
        }
    }
}
//Custom Function
extension UserViewController{
    func fetchUserList(){
        userListVM.requestList { (result) in
            switch result {
            case .success(_) :
                  DispatchQueue.main.async {
                    self.tableView.reloadData()
                  }
                            
            case .failure(_):
                break
          }
       }
    }
}
