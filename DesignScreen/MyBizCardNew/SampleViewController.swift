//
//  SampleViewController.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/27/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class SampleViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let titleArr = ["Hiii","o set automatic dimension for row height & estimated row height, ensure following steps to make, auto dimension effective for cell/row height layout.","This quickstart describes how to set up Firebase Crashlytics so that you can get comprehensive crash reports in the","Thanks you"]
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.estimatedRowHeight = 600.0
        self.tableView.rowHeight = UITableView.automaticDimension
   
    }
}
extension SampleViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SampleTableViewCell", for: indexPath) as! SampleTableViewCell
        cell.ConfigureCell(title: titleArr[indexPath.row])
        return cell
    }
    
    
}
