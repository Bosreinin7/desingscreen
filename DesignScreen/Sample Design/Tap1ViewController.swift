//
//  Tap1ViewController.swift
//  DesignScreen
//
//  Created by Srei Nin on 10/7/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class Tap1ViewController: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    
    var blockHeight:((CGFloat)->CGFloat)?
    override func viewDidLoad() {
        super.viewDidLoad()
   
    }

}
extension Tap1ViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        return cell
    }
  
 
}

