//
//  DrivingViewController.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/1/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit
import PageMenu
class DrivingViewController: UIViewController {
    //MARK:- IBOUTLET--------------
    @IBOutlet weak var kilometerTableView: UITableView!
    @IBOutlet weak var profileImgHeightconstraints: NSLayoutConstraint!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var plusView: UIView!
    @IBOutlet weak var subItemView: UIView!
    @IBOutlet weak var overlayView: UIView!
    //MARK:- Variable-----------
    var imageViewNormalHeight : CGFloat!
    var numRow = 3
    var plusbutton:UIButton!
    var subItemWidth:CGFloat = 75.0
    var subItemHeight:CGFloat = 75.0
    var itemSpace = 15
    var itemViews:UIView = UIView()
    var items:[UIButton]!
    var itemsTitle = ["Hello","Sreynin"]
    var itemTitleLabel:[UILabel] = [UILabel]()
    private lazy var popUpFilterView:FilterViewP = {
        let yposition = self.filterView.frame.origin.y
        return FilterViewP(xPosition: 0, yPosition: yposition, parentView: self.view)
     }()
    
    private var overLayView:UIControl = UIControl()
    
    //MARK:- IBAction
    @IBAction func filterClicked(_ sender: UIButton) {
        let yPosition = self.filterView.frame.origin.y
        popUpFilterView.frame.origin.y = yPosition
        popUpFilterView.show()
        popUpFilterView.layer.zPosition = 1
           
    }
    
    @IBAction func searchClicked(_ sender: UIButton) {
        
    }
    @IBAction func moreAction(_ sender: UIButton) {
        if let popover = animateViewController(storyboard: "PopOver", identifier: "PopOverSB") as? PopUpViewController{
            popover.menuArr = [.Changebusiness,.logout]
            self.present(popover, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func plusBtnAction(_ sender: UIButton) {
           sender.isSelected = sender.isSelected ? false : true
           sender.pulsate()
        
        UIView.animate(withDuration: 0.4) {
            if sender.isSelected {
//                self.updateItemsConstraint(sender)
                self.overlayView.alpha = 0.7
                self.view.insertSubview(self.overLayView as UIView, belowSubview: self.plusView)
                self.plusButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 4))
                self.itemViews.isHidden = false
                self.showItemLabels()
                self.itemViews.layoutIfNeeded()
             }else{
                self.plusButton.transform = CGAffineTransform.identity
                self.overlayView.alpha = 0
                self.itemViews.isHidden = true
//                self.updateItemsConstraint(sender)
                self.itemViews.layoutIfNeeded()
                self.hideItemLabels()
                            
            }
        }          
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraintAndInsetTableView: do {
            imageViewNormalHeight = self.profileImgHeightconstraints.constant
            kilometerTableView.separatorInset = .zero
            kilometerTableView.contentInset = UIEdgeInsets(top: imageViewNormalHeight, left: 0, bottom: 0, right: 0)
            kilometerTableView.scrollIndicatorInsets = UIEdgeInsets(top: imageViewNormalHeight, left:0, bottom: 0, right: 0)
        }
        setupGestureReconizerToOverlayView: do {
            let tapoverlay = UITapGestureRecognizer(target: self, action: #selector(overlayDidClicked(_sender:)))
            self.overlayView.addGestureRecognizer(tapoverlay)
            self.overlayView.isUserInteractionEnabled  = true
        }
        setupButtonItems: do {
            items = [setUpItemViews(strimg: "register_icon_btn", itemnum: 1),
                     setUpItemViews(strimg: "register_icon_btn", itemnum: 2)]
            
            for (i,item) in items.enumerated() {
                AddItemToViewitems(item: item, itemnum: i + 1)
            }
        }
        setupLabelTitleItems: do {
            for (i,title) in itemsTitle.enumerated() {
                self.itemTitleLabel.append(setupItemTitleLabel(strTitle: title, itemIndex: i + 1))
            }
             self.hideItemLabels()
        }
        self.itemViews.isHidden  = true
        self.overlayView.alpha = 0
        self.kilometerTableView.register(UINib(nibName: "NodataTableViewCell", bundle: nil), forCellReuseIdentifier: "NodataCell")
    }
}
extension DrivingViewController {
    @objc func overlayDidClicked(_sender:UITapGestureRecognizer){
        UIView.animate(withDuration: 0.1) {
            self.plusButton.transform = CGAffineTransform.identity
            self.overlayView.alpha = 0
            self.itemViews.isHidden = true
            self.subItemView.isHidden = true
                                   
        }
                          
    }
}
extension DrivingViewController{
    func  setOverlayView() {
        setOverlayFrame()
        overLayView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.7)
        overLayView.alpha = 0.8
        overLayView.layer.zPosition = 1
        overLayView.isUserInteractionEnabled = true
        
    }
    func setOverlayFrame(){
        if let superview = self.view {
            overLayView.frame = CGRect(x: 0, y: 0, width: superview.bounds.width, height: superview.bounds.height)
        }
    }
    func setUpItemViews(strimg:String,itemnum:Int) -> UIButton{
        let items = UIButton()
        items.frame = CGRect(x: 0 , y: 0, width:self.subItemWidth, height: self.subItemHeight)
        items.setImage(UIImage(named: strimg), for: .normal)
//        items.setTitle(String(itemnum), for: .normal)
//        items.setTitleColor(.red, for: .normal)
//        items.backgroundColor = .lightGray
        self.itemViews.frame = CGRect(x:  self.plusView.frame.origin.x , y: self.plusView.frame.origin.y - ((self.subItemWidth * CGFloat(itemnum) + CGFloat(itemSpace * itemnum))), width: self.plusView.bounds.width, height: ((items.bounds.height + CGFloat(itemSpace)) * CGFloat(itemnum)))
        self.view.addSubview(itemViews)
        
        items.widthAnchor.constraint(equalToConstant: 75).isActive = true
        items.heightAnchor.constraint(equalToConstant: 75).isActive = true
        return items
    }
    func AddItemToViewitems(item:UIView,itemnum: Int){
         let firstItemHeight = (self.subItemHeight * CGFloat(itemnum)) + CGFloat(itemSpace)
         let itemsHeight     = (self.subItemHeight * CGFloat(itemnum)) + (CGFloat(itemnum) * CGFloat(itemSpace))
        item.translatesAutoresizingMaskIntoConstraints = false
        item.frame = CGRect(x: (self.subItemWidth / 7 ) , y: (self.itemViews.bounds.height - (itemnum == 1 ? firstItemHeight : itemsHeight )), width:self.subItemWidth, height: self.subItemHeight)
        self.itemViews.addSubview(item)
        
        item.bottomAnchor.constraint(equalTo:self.itemViews.bottomAnchor, constant: (itemnum == 1 ? -15 : -(CGFloat(15 * itemnum) + (self.subItemHeight * CGFloat((itemnum - 1)))))).isActive = true
        item.leadingAnchor.constraint(equalTo:self.itemViews.leadingAnchor, constant: 10).isActive = true
       
    }
    func setupItemTitleLabel(strTitle:String,itemIndex:Int) -> UILabel{
        let itemLabel = UILabel()
        itemLabel.text = strTitle
        itemLabel.numberOfLines = 0
        itemLabel.textColor = .white
        itemLabel.textAlignment = .right
        itemLabel.frame = CGRect(x: self.plusView.frame.origin.x - 200, y: (self.itemViews.frame.origin.y + self.itemViews.bounds.height) - ((self.subItemHeight * CGFloat(itemIndex)) + (itemIndex == 1 ? 0 : CGFloat(itemSpace) * CGFloat(itemIndex - 1))) , width: 200, height: 30)
        self.view.addSubview(itemLabel)
        return itemLabel
    }
    func hideItemLabels(){
        for lbl in itemTitleLabel {
            lbl.isHidden = true
        }
    }
    func showItemLabels(){
      for lbl in itemTitleLabel {
                 lbl.isHidden = false
            }
    }
    func updateItemsConstraint(_ sender:UIButton){
        var firtView:UIButton = UIButton()
        
            for (i,item) in items!.enumerated(){
//                let itemnum = i + 1
                if i == 0 {
                    firtView = item
                }else{
                    if !sender.isSelected {
                        self.itemViews.translatesAutoresizingMaskIntoConstraints = false
                    item.bottomAnchor.constraint(equalTo:firtView.bottomAnchor).isActive = true
                    }
            }
        }
    }
}

extension DrivingViewController:UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numRow == 0 ? 1 : numRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if numRow > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DrivingCell", for: indexPath) as! DrivingTableViewCell
            return cell
        }else{
            let nodataCell = tableView.dequeueReusableCell(withIdentifier: "NodataCell", for: indexPath) as! NodataTableViewCell
            return nodataCell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // To Show NodataView when no data
        if numRow > 0 {
            return  55
        }else{
            return tableView.layer.bounds.height - self.profileImgHeightconstraints.constant
        }
    }
}
extension DrivingViewController:UITableViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y =  imageViewNormalHeight - (scrollView.contentOffset.y +  imageViewNormalHeight)
        let height = min(max(y, 0), imageViewNormalHeight)
        self.profileImgHeightconstraints.constant = height
        view.layoutIfNeeded()
    }
    
}
