//
//  MyBizCardNewVC.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/27/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit
import WebKit
class MyBizCardNewVC: UIViewController,WKNavigationDelegate {
    
   
    //MARK: - IBOUTLET
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var ListView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //MARK: - Variable
    var begin:Bool = false
    var index      = 0
    let item = 10
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonClass.setStatusBarBackground(UIColor(r: 50,g: 103,b: 227,alpha: 1.0)!)
        self.ListView.layer.cornerRadius = 10
        
        //Auto Scroll CollectionView
        self.startTimer()
    }
  
}
//MARK: - Custom Function
extension MyBizCardNewVC{
    
    @objc func scrollToNextCell(){
        if let coll  = collectionView {
            for cell in coll.visibleCells {
                let indexPath:IndexPath? = coll.indexPath(for: cell)
                if (indexPath?.row)! < item {
                    var indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .centeredHorizontally, animated: true)
                    if indexPath1!.row == item {
                        let indexPath2: IndexPath?
                        indexPath2 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                            coll.scrollToItem(at: indexPath2!, at: .centeredHorizontally, animated: false)
                        pageControl.currentPage = 0
                        }
                }
            }
        }
        
    }
    func startTimer(){
           Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
    }
}

//MARK:- CollectionView Delegate
extension MyBizCardNewVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = item
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemcell = collectionView
        .dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! collectionCell
        itemcell.backgroundColor = .random()
        itemcell.configureCell(title: String(indexPath.row + 1))
        return itemcell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
        let width  = view.frame.width
        let height = collectionView.frame.height
               return CGSize(width: width, height: height)
       }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int((collectionView.contentOffset.x / collectionView.contentSize.width) * CGFloat(item))
        if collectionView.contentSize.width == collectionView.contentOffset.x + view.frame.width {
            begin = true
        }
    }
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollViewDidEndDecelerating(scrollView)
    }
}
extension MyBizCardNewVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return item
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableViewCell", for: indexPath) as! listTableViewCell
        cell.configureCells(title: String(indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
}
