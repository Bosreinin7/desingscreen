//
//  SampleTableViewCell.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/27/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class SampleTableViewCell: UITableViewCell {
    @IBOutlet weak var textlbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func ConfigureCell(title:String){
        self.textlbl.text = title
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
