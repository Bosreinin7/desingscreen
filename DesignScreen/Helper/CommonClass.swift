//
//  CommonClass.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/15/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import Foundation

class CommonClass : NSObject {
    class func getViewControllerFrom(_ storyboardID:String , viewControllerID:String)->UIViewController!{
     
     let storyboard = UIStoryboard(name: storyboardID, bundle: nil)
     let vc = storyboard.instantiateViewController(withIdentifier: viewControllerID)
     return vc;
  }
    class func setStatusBarBackground(_ color: UIColor) {
        if #available(iOS 13.0, *) {
            DispatchQueue.main.async {
                let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                statusBar.backgroundColor = color
                
                UIApplication.shared.keyWindow?.addSubview(statusBar)
            }
        }
        else {
            if let statusBarView = (UIApplication.shared.value(forKey: "statusBarWindow") as? UIWindow)?.value(forKey: "statusBar") as? UIView {
                if statusBarView.responds(to: #selector(setter: UIView.backgroundColor)) {
                    DispatchQueue.main.async {
                        statusBarView.backgroundColor = color
                    }
                }
            }
        }
    }
    class func setNavigationBackground(_ target:AnyObject,red:CGFloat,green:CGFloat,blue:CGFloat,alpha:CGFloat){
        guard target is UIViewController == true else {
            return
        }
        let viewController:UIViewController = target as! UIViewController
        viewController.navigationController?.navigationBar.barTintColor = UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
        viewController.navigationController?.navigationBar.tintColor = UIColor.white;
        viewController.navigationController?.navigationBar.isTranslucent = false
        viewController.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
}
/*
 TODO: Check Device Type for Simulator & Real Device
 */
struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPHONE_XS         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPHONE_X_MAX      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
}
