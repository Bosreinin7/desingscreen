//
//  MyJSON.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/20/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import Foundation

struct MyJSON {
    static func prettyPrint(value:NSObject) -> String{
        if JSONSerialization.isValidJSONObject(value){
            if let data = try? JSONSerialization.data(withJSONObject: value, options: .prettyPrinted) {
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }
        }
        return ""
    }
    static func jsonToDic(data:Data) ->NSDictionary {
        guard let dic:NSDictionary = try? JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else {
            return [:]
        }
        return dic
    }
}
