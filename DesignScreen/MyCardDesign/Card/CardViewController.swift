//
//  CardViewController.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/14/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit
import ScaledVisibleCellsCollectionView
class CardViewController: UIViewController {
    //MARK:- IBOUTLET==================
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var heightCollectionViewConstaint: NSLayoutConstraint!
    @IBOutlet weak var totalCardLabel: UILabel!
    @IBOutlet weak var buttonCorperation: UIButton!
    //MARK:- IBVARIABLE=================
     var fromSavedCard           : Bool = true
     var mainScroll              : UIScrollView!
    let colorVeiw:[UIColor] = [.clear,.clear,.clear,.clear,.clear]
    //MARK:- View Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        
//        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            self.collectionView.scaledVisibleCells()
//        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonCorperation.layer.borderWidth = 1
        self.buttonCorperation.layer.borderColor = UIColor.blue.cgColor
        self.buttonCorperation.layer.cornerRadius = 5
        self.collectionView.allowsSelection = false
//        if DeviceType.IS_IPHONE_4_OR_LESS {
//                    bottomMainView.constant = -75.0
//                    mainScroll = UIScrollView()
//                }
                
                if DeviceType.IS_IPHONE_6P {
                    heightCollectionViewConstaint.constant = heightCollectionViewConstaint.constant + 60
                    collectionView.setScaledDesginParam(scaledPattern: .HorizontalCenter, maxScale: 1.1, minScale: 0.8, maxAlpha: 1.0, minAlpha: 0.9)
                }
                else if DeviceType.IS_IPHONE_6 {
                    heightCollectionViewConstaint.constant = heightCollectionViewConstaint.constant + 16
                    collectionView.setScaledDesginParam(scaledPattern: .HorizontalCenter, maxScale: 1.1, minScale: 0.8, maxAlpha: 1.0, minAlpha: 0.9)
                }
                else if DeviceType.IS_IPHONE_X || DeviceType.IS_IPHONE_X_MAX {
                    heightCollectionViewConstaint.constant = heightCollectionViewConstaint.constant + 35
                    collectionView.setScaledDesginParam(scaledPattern: .HorizontalCenter, maxScale: 1.15, minScale: 0.90, maxAlpha: 1.0, minAlpha: 0.9)
                }
        //        else if DeviceType.IS_IPHONE_5 {
        //            heightCollectionView.constant = heightCollectionView.constant + 15
        //            collectionView.setScaledDesginParam(scaledPattern: .HorizontalCenter, maxScale: 0.9, minScale: 0.6, maxAlpha: 1.0, minAlpha: 0.9)
        //        }
                else {
                    collectionView.setScaledDesginParam(scaledPattern: .HorizontalCenter, maxScale: 1.03, minScale: 1.0, maxAlpha: 1.0, minAlpha: 0.9)
                }
       
        pageControl.numberOfPages = colorVeiw.count
    }
    override func viewDidLayoutSubviews() {
          collectionView.scaledVisibleCells()
             var insets = self.collectionView.contentInset
        let value = (self.view.frame.size.width - (self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize.width) * 1.5
             insets.left = value
             insets.right = value
             self.collectionView.contentInset = insets
        self.collectionView.decelerationRate = UIScrollView.DecelerationRate.fast;
    }
}
extension CardViewController:UICollectionViewDataSource  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colorVeiw.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        cell.view.backgroundColor = colorVeiw[indexPath.row]
        collectionView.scaledVisibleCells()
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
         if let ip = collectionView.indexPathForItem(at: center) {
            // page control maximum is 5
            if (ip as NSIndexPath).row <= 5 {
            self.pageControl.currentPage = (ip as NSIndexPath).row
            }
            else {
            self.pageControl.currentPage = 5
            }
            self.totalCardLabel.text = String(self.pageControl.currentPage + 1) + "/" + String(colorVeiw.count)
        }
         collectionView.scaledVisibleCells()
         
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
         collectionView.scaledVisibleCells()
        if let ip = collectionView.indexPathForItem(at: center) {
            self.collectionView.selectItem(at: ip, animated: true, scrollPosition: UICollectionView.ScrollPosition())
            self.collectionView(self.collectionView, didSelectItemAt: ip)
            self.collectionView.scrollToItem(at: ip, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
            
        }
       
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
          if !decelerate { collectionView.delegate?.scrollViewDidEndDecelerating!(scrollView)
          }
      }
}

extension CardViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scaledVisibleCells()
    }
    
}
