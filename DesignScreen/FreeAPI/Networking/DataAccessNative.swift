//
//  DataAccessNative.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/20/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import Foundation

class DataAccessNative {
    
    func request<I: Encodable,O: Decodable>(
                                            apikey      :String,
                                            httpMethod  : RequestMethod,
                                            body        :I?,
                                            responseType:O.Type,
                                            completion  : @escaping (Swift.Result<O, NSError>) -> Void){
        
        var request = URLRequest.init(url: URL(string: APIKey.url.rawValue + apikey)!)
        request.httpBody = body? .asJSONString().data(using: .utf8)
        request.httpMethod = httpMethod.rawValue
        request.timeoutInterval = 60
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error.debugDescription)
                DispatchQueue.main.async {
                    let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey:error?.localizedDescription ?? ""])
                    completion(.failure(error))
                }
                 return
            }
            
            guard let data = data else {
                print("No data response----")
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey:"No Data Response"])
                completion(.failure(error))
                return
            }
            
            //For Print log
            let dic = MyJSON.jsonToDic(data: data)
            
            print("""
                
                ---------Response--------------
                requset url:\(String(describing: request.url))
                responseData: \(MyJSON.prettyPrint(value: dic))
                """)
            
           //Decode from data and Mapping with Model
         
            guard let decodefromdata = try? JSONDecoder().decode(O.self, from: data) else {
                print("Error Mapping data")
                
                let error = NSError(domain: "ClientError", code:168  , userInfo: [NSLocalizedDescriptionKey:"Oop someting when worng"])
                completion(.failure(error))
                return
            }
            
            //Callback Object model
            completion(.success(decodefromdata))
        }.resume()
        
        
    }
}
