//
//  File.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/20/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import Foundation

enum RequestMethod:String{
    case get   = "GET"
    case post  = "POST"
}

enum APIKey:  String {
    case url        = "https://reqres.in/api/"
    case list_user  = "users?page="
    
}

