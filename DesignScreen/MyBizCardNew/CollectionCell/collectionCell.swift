//
//  CollectionViewCell.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/27/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class collectionCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    func configureCell(title:String){
        self.titleLabel.text = title
    }
}
