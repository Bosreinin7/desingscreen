//
//  UserListVM.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/20/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import Foundation

class UserListVM{
    var pages = 1
    var isLastpage = false
    
    var data = [UserListModel.Response.data]()
    
    func requestList(completion:@escaping(Swift.Result<UserListModel.Response,NSError>)->Void){
        API.native.request(
            apikey          : APIKey.list_user.rawValue + String(pages),
            httpMethod      : .get,
            body            : nil as String?,
            responseType    : UserListModel.Response.self) { (result) in
             
                switch result {
                case .success(let response):
                    self.data.append(contentsOf: response.data)
                    self.pages += 1
                    self.isLastpage = self.data.count == response.total
                    completion(.success(response))
                    
                case .failure(let error):
                    completion(.failure(error))
                }
        }
    }
}
