//
//  ViewController.swift
//  DesignScreen
//
//  Created by Srei Nin on 9/11/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit
import PageMenu

class ViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var bodyHeightConstraint: NSLayoutConstraint!
    var subHeaderContentOffset: CGFloat = 0
    var bottomCoordinateY:CGFloat = 0
    var pagemenu:CAPSPageMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.bottomCoordinateY = self.scrollView.contentOffset.y + self.scrollView.frame.size.height
        if #available(iOS 11, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        var controllerArray:[UIViewController] = []
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 30))
        let imgView = UIImageView(image: UIImage(named: "home"))
        view.addSubview(imgView) 
        let controller1 = storyboard?.instantiateViewController(withIdentifier: "tap1VC") as! Tap1ViewController
        controller1.navigationItem.titleView = imgView
       
        controllerArray.append(controller1)
        let controller2 = storyboard?.instantiateViewController(withIdentifier: "tap2VC") as! Tap2ViewController
        controller2.title = "Payment"
        controllerArray.append(controller2)
        // Customize menu (Optional)
               let parameters: [CAPSPageMenuOption] = [
                   .scrollMenuBackgroundColor(UIColor.orange),
                   .viewBackgroundColor(UIColor.white),
                   .selectionIndicatorColor(UIColor.white),
                   .unselectedMenuItemLabelColor(UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.4)),
                   .menuItemFont(UIFont(name: "HelveticaNeue", size: 20.0)!),
                   .menuHeight(44.0),
                   .menuMargin(20.0),
                   .selectionIndicatorHeight(0.0),
                   .bottomMenuHairlineColor(UIColor.orange),
                   .menuItemWidthBasedOnTitleTextWidth(true),
                   .selectedMenuItemLabelColor(UIColor.white)
               ]
        // Initialize scroll menu
        pagemenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.bodyView.frame.width, height: self.view.frame.height - 60.0), pageMenuOptions: parameters)
              
        self.bodyView.addSubview(pagemenu!.view)
       
       
    }
   
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 200 {
             self.navigationView.backgroundColor = UIColor(cgColor: CGColor(srgbRed: 255/255, green: 45/255 , blue: 85/255, alpha: 1))
        }else {
            self.navigationView.backgroundColor = .clear
       
        }
    print("scrollView.contentOffset.y=====\(scrollView.contentOffset.y)")
    print("=======\(self.bottomCoordinateY - (self.subHeaderView.frame.size.height + self.bodyView.frame.size.height))")
                
    }
}

extension ViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        return cell
    }
    
    

}
