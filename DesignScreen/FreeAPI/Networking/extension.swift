//
//  extension.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/20/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import Foundation
extension Encodable {
    
    var encodeToDic: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
    func asJSONString() -> String {
        let jsonEncoder = JSONEncoder()
        let jsonData    = try? jsonEncoder.encode(self)
        let jsonString  = String(data: jsonData ?? Data(), encoding: .utf8)
        return jsonString ?? ""
    }
}
