//
//  UserListModel.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/20/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import Foundation

struct UserListModel {
    struct Request:Encodable{
        
    }
    struct Response:Decodable {
        let page        : Int
        let per_page    : Int
        let total_pages : Int
        let total       : Int
        let data        :[data]
    struct data:Decodable {
        let id          : Int
        let email       : String
        let first_name  : String
        let last_name   : String
        let avatar      : String
    }
  }
}
