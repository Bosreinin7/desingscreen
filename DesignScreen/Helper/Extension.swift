//
//  Extension.swift
//  DesignScreen
//
//  Created by Srei Nin on 10/30/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import Foundation
import UIKit
extension NSNotification.Name {
    static let MyName = NSNotification.Name("MyNotofication")
}
extension UIViewController{
    
    func animateViewController(storyboard: String, identifier: String) -> UIViewController {
        let vc = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        return vc
    }

}
extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
    static func random() -> UIColor {
           return UIColor(red:   .random(),
                          green: .random(),
                          blue:  .random(),
                          alpha: 1.0)
       }
}
extension UIButton {
    func pulsate() {
    let pulse = CASpringAnimation(keyPath: "transform.scale")
    pulse.duration = 0.4
    pulse.fromValue = 0.98
    pulse.toValue = 1.0
    pulse.initialVelocity = 0.5
    pulse.damping = 1.0
    layer.add(pulse, forKey: nil)
    }
    func flash() {
    let flash = CABasicAnimation(keyPath: "opacity")
    flash.duration = 0.3
    flash.fromValue = 1
    flash.toValue = 0.1
    flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    flash.repeatCount = 2
    layer.add(flash, forKey: nil)
    }
    
    private func actionHandleBlock(action:(() -> Void)? = nil) {
           struct __ {
               static var action :(() -> Void)?
           }
           if action != nil {
               __.action = action
           } else {
               __.action?()
           }
       }

       @objc private func triggerActionHandleBlock() {
           self.actionHandleBlock()
       }

    func actionHandle(controlEvents control :UIControl.Event, ForAction action:@escaping () -> Void) {
        self.actionHandleBlock(action: action)
        self.addTarget(self, action: #selector(triggerActionHandleBlock), for: control)
       }
}
/*
 TODO : Set color
 */
extension UIColor {
    //Convert to RGB without Alpha
    public convenience init?(r:CGFloat,g:CGFloat,b:CGFloat){
        self.init(red:r/255.0,green: g/255.0,blue: b/255.0,alpha: 1.0)
        return
    }
    
    //Convert to RGB with alpha
    public convenience init?(r:CGFloat,g:CGFloat,b:CGFloat,alpha:CGFloat) {
        self.init(red:r/255.0,green: g/255.0,blue: b/255.0,alpha: alpha)
        return
    }
    
    //Convert to Hex
    public convenience init?(hexString:String){
        let hexString:NSString = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString
        let scanner            = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    public class func StatusBarColor() -> UIColor{
        return UIColor(r: 63 , g: 127, b: 233, alpha:1)!
    }
    
    public class func receiptNomalBgColor()-> UIColor {
        return UIColor.white
    }
    
    public class func receiptNewBgColor()-> UIColor{
        return UIColor.init(hexString: "eef4ff")!
    }
    
    public class func receiptTextColor_black()-> UIColor{
        return UIColor.init(r: 24/255.0, g: 24/255.0, b: 27/255.0, alpha: 1.0)!
    }
    public class func receiptTextColor_grey()->UIColor{
        return UIColor.init(r: 147, g: 147, b: 147)!
    }
    
}
