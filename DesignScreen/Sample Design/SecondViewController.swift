//
//  SecondViewController.swift
//  DesignScreen
//
//  Created by Srei Nin on 9/20/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.post(name: .MyName, object: nil)
    }
}
extension SecondViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.tableView.separatorColor = .clear
        if indexPath.row == 0 {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "headercell", for: indexPath) as! TableViewCell
                  return headerCell
            
        }else if indexPath.row == 1{
            let detailHeaderCell = tableView.dequeueReusableCell(withIdentifier: "detailHeader", for: indexPath) as! TableViewCell
            return detailHeaderCell
            
        }else if indexPath.row == 2 {
            let tapMenuCell = tableView.dequeueReusableCell(withIdentifier: "tapMenu", for: indexPath) as! TableViewCell
            tapMenuCell.segmentControl.layer.cornerRadius = 0
            tapMenuCell.segmentControl.clipsToBounds = true
            tapMenuCell.segmentControl.backgroundColor = .white
        
           
            
            return tapMenuCell
        }else {
              let tapMenuInfoCell = tableView.dequeueReusableCell(withIdentifier: "tapInfo", for: indexPath) as! TableViewCell
              return tapMenuInfoCell
        }
      
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if  scrollView.contentOffset.y > 90 {
            self.navigationView.backgroundColor = UIColor(red: 52/255, green: 199/255, blue: 98/255, alpha: 1)
        }else{
            self.navigationView.backgroundColor = .clear
        }
        if scrollView.contentOffset.y > 155 {
            tableView.scrollsToTop = false
        }
    }
     
}
