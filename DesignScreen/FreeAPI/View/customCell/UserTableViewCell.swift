//
//  UserTableViewCell.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/21/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit
import Kingfisher
class UserTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var email: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    func configureCell(data: UserListModel.Response.data)  {
        self.firstName.text = data.first_name
        self.lastName.text  = data.last_name
        self.email.text     = data.email
        self.userImg.kf.setImage(with: URL(string: data.avatar))
    }
  
}
