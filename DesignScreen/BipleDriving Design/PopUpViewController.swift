//
//  PopUpViewController.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/5/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

enum MenuTitle:String{
    case Changebusiness = "ChangeB"
    case logout = "Logout"
}

class PopUpViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var popUpViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var customView: UIView!
    
    var menuArr:[MenuTitle] = []
    
    
    @IBAction func dismissPopOver(_ sender: UIButton) {
        animationHeightViewWhenClose: do{
            UIView.animate(withDuration: 0.3) {
                self.popUpViewHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
            }
        }
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = .none
        self.customView.layer.cornerRadius = 5
        self.customView.layer.shadowColor = UIColor.red.cgColor
    }
}
extension PopUpViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopUpCell", for: indexPath) as! TableViewCell
        cell.popUpNameLabel.text = menuArr[indexPath.row].rawValue
        
        return cell
    }
}
extension PopUpViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        animationHeightViewWhenClose: do {
            UIView.animate(withDuration: 0.3) {
                self.popUpViewHeightConstraint.constant = tableView.contentSize.height
                self.view.layoutIfNeeded()
            }
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42.0
    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! TableViewCell
        cell.popUpNameLabel.textColor = UIColor(rgb:0x4286f5)
        cell.contentView.backgroundColor = UIColor(named: "f3f3f3")
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
           let cell = tableView.cellForRow(at: indexPath) as! TableViewCell
           cell.popUpNameLabel.textColor = UIColor(named: "263238")
           cell.contentView.backgroundColor = .white
       }
}
