//
//  TableViewCell.swift
//  DesignScreen
//
//  Created by Srei Nin on 9/20/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var contentMenuView: UIView!
    @IBOutlet weak var popUpNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
