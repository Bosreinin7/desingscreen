//
//  MyCardViewController.swift
//  DesignScreen
//
//  Created by Srei Nin on 11/14/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class MyCardViewController: UIViewController {
    //MARK: - IBOUTLET----------
    @IBOutlet weak var titleLabel: UILabel!
    
   
     //MARK: - VARIABLES----------
    var carbonTabSwapNavigation : CarbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var arrTitles:[String] = []
    var arrViewControllers = [["CardSB","CardViewController"],["HistorySB","HistoryViewController"]]
    var imageIcons : NSArray = [
            UIImage(named: "card_in_use")!,
            UIImage(named: "comments")!,
            UIImage(named: "dateTime")!,
            UIImage(named: "home")!,
        ]
    override func viewDidLoad() {
        super.viewDidLoad()
        arrTitles = ["MyCard","History"]
        CommonClass.setStatusBarBackground(UIColor(r: 50,g: 103,b: 227,alpha: 1.0)!)
        setupNavigationBar: do {
            customNavigationStyle()
            CommonClass.setNavigationBackground(self, red: 66, green: 134, blue: 245, alpha: 1)
        }
     
        initCarbonTabSwipeNavigation()
        
    }
    
    func initCarbonTabSwipeNavigation(){
        self.carbonTabSwapNavigation = CarbonTabSwipeNavigation(items: imageIcons as [AnyObject], delegate: self)
              self.carbonTabSwapNavigation.insert(intoRootViewController: self)
              
              self.carbonTabSwapNavigation.setTabBarHeight(49) //30
              self.carbonTabSwapNavigation.setIndicatorHeight(2)
              self.carbonTabSwapNavigation.setIndicatorColor(UIColor.white)
              self.carbonTabSwapNavigation.pagesScrollView?.isScrollEnabled = false
              
              let carbonSegmentWidth = self.view.frame.width / CGFloat(imageIcons.count)
              let segment2 : CGFloat = 1
              let segment3 : CGFloat = 1
              let segment4 : CGFloat = 1
          //--Custimize segmented control
                 self.carbonTabSwapNavigation.carbonSegmentedControl?.setWidth(carbonSegmentWidth, forSegmentAt: 0)
                 self.carbonTabSwapNavigation.carbonSegmentedControl?.setWidth(carbonSegmentWidth + segment2, forSegmentAt: 1)
                 self.carbonTabSwapNavigation.carbonSegmentedControl?.setWidth(carbonSegmentWidth + segment3, forSegmentAt: 2)
                 self.carbonTabSwapNavigation.carbonSegmentedControl?.setWidth(carbonSegmentWidth + segment4, forSegmentAt: 3)
                 self.carbonTabSwapNavigation.setNormalColor(UIColor.white.withAlphaComponent(0.5))
                 
                 self.carbonTabSwapNavigation.setSelectedColor(UIColor.white.withAlphaComponent(1.0))
                 
                 self.carbonTabSwapNavigation.toolbarHeight.constant = 48.0
                 self.carbonTabSwapNavigation.carbonSegmentedControl?.backgroundColor = UIColor(red: 66/255, green: 134/255, blue: 245/255, alpha: 1)
                 
                 self.carbonTabSwapNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
                 
    }
   @objc func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAtIndex index: UInt) -> UIViewController {
       /*
        _--Set navigation menu
        
        **Set  viewcontrollers for
        **tab  swipe nagivation
        **goes here
        */
       let arr = arrViewControllers[Int(index)]
    return (CommonClass.getViewControllerFrom( arr[0] , viewControllerID: arr[1]))
   }
   @objc func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAtIndex index: UInt) {
       
    titleLabel.text = arrTitles[Int(index)]
       
   }
   
   @objc func barPositionForCarbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation) -> UIBarPosition {
       return UIBarPosition.top ;
   }
 //MARK:------customNavigationStyle
   private func customNavigationStyle() {
       
       self.navigationController?.navigationBar.transform  = CGAffineTransform(translationX: 0, y: 0)
       //--Hide line buttom of UINavigation
       let img = UIImage()
       self.navigationController?.navigationBar.shadowImage = img
       self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
  
   }
    //MARK: remove navigation bar
      private func removeNavigationStyle(){
          self.navigationController?.navigationBar.isHidden = true
      }
      
}
